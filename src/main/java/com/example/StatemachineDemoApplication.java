package com.example;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.messaging.support.GenericMessage;
import org.springframework.statemachine.StateMachine;

@SpringBootApplication
public class StatemachineDemoApplication implements CommandLineRunner {
    @Autowired
    private StateMachine<OrderStatus, OrderEvent> stateMachine;

    @Override
    public void run(String... strings) throws Exception {
        Map<String, Object> headers = new HashMap<>();
        OrderDTO order = new OrderDTO("01", "order-01", new Date());
        headers.put("order", order);

        stateMachine.start();
        stateMachine.sendEvent(new GenericMessage<>(OrderEvent.CREATE, headers));
        stateMachine.sendEvent(new GenericMessage<>(OrderEvent.CANCEL, headers));
        stateMachine.stop();

        order = new OrderDTO("02", "order-02", new Date());
        headers.put("order", order);
        stateMachine.start();
        stateMachine.sendEvent(new GenericMessage<>(OrderEvent.CREATE, headers));
        stateMachine.sendEvent(new GenericMessage<>(OrderEvent.PAY, headers));
        stateMachine.sendEvent(new GenericMessage<>(OrderEvent.DELIVER, headers));
        stateMachine.sendEvent(new GenericMessage<>(OrderEvent.RECEIVE, headers));
        stateMachine.stop();
    }

    public static void main(String[] args) {
        SpringApplication.run(StatemachineDemoApplication.class, args);
    }
}
