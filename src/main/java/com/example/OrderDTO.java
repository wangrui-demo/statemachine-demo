package com.example;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * Created by Administrator on 2017/3/24.
 */
@Data
@AllArgsConstructor
public class OrderDTO {
    private String id;

    private String billCode;

    private Date timeCreated;
}
