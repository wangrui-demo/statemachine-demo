package com.example;

/**
 * The OrderStatus enumeration.
 */
public enum OrderStatus {
    NONE,

    /**
     * 待支付
     */
    UNPAID,

    /**
     * 已取消
     */
    CANCELED,

    /**
     * 已支付
     */
    PAID,

    /**
     * 已发货
     */
    DELIVERED,

    /**
     * 已完成
     */
    FINISHED
}
