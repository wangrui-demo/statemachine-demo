package com.example;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.statemachine.StateContext;
import org.springframework.statemachine.annotation.OnTransition;
import org.springframework.statemachine.annotation.WithStateMachine;

/**
 * Created by Administrator on 2017/3/23.
 */
@WithStateMachine
public class OrderEventHandler {
    private Logger logger = LoggerFactory.getLogger(getClass());

    @OnTransition(target = "UNPAID")
    public void create(StateContext<OrderStatus, OrderEvent> stateContext) {
        logger.info("订单创建，待支付：" + stateContext.getMessageHeader("order"));
    }

    @OnTransition(source = "UNPAID", target = "CANCELED")
    public void cancel(StateContext<OrderStatus, OrderEvent> stateContext) {
        logger.info("订单已取消：" + stateContext.getMessageHeader("order"));
    }

    @OnTransition(source = "UNPAID", target = "PAID")
    public void pay(StateContext<OrderStatus, OrderEvent> stateContext) {
        logger.info("用户完成支付，待发货：" + stateContext.getMessageHeader("order"));
    }

    @OnTransition(source = "PAID", target = "DELIVERED")
    public void deliver(StateContext<OrderStatus, OrderEvent> stateContext) {
        logger.info("商家已发货，待收货：" + stateContext.getMessageHeader("order"));
    }

    @OnTransition(source = "DELIVERED", target = "FINISHED")
    public void receive(StateContext<OrderStatus, OrderEvent> stateContext) {
        logger.info("用户已收货，订单完成：" + stateContext.getMessageHeader("order"));
    }
}
