package com.example;

import java.util.EnumSet;

import org.springframework.context.annotation.Configuration;
import org.springframework.statemachine.config.EnableStateMachine;
import org.springframework.statemachine.config.EnumStateMachineConfigurerAdapter;
import org.springframework.statemachine.config.builders.StateMachineStateConfigurer;
import org.springframework.statemachine.config.builders.StateMachineTransitionConfigurer;

/**
 * 订单状态机配置
 *
 * @author wangruiv on 2017-3-23 15:18:31
 */
@Configuration
@EnableStateMachine
public class OrderStateMachineConfig extends EnumStateMachineConfigurerAdapter<OrderStatus, OrderEvent> {
    @Override
    public void configure(StateMachineStateConfigurer<OrderStatus, OrderEvent> states) throws Exception {
        states.withStates().initial(OrderStatus.NONE).states(EnumSet.allOf(OrderStatus.class));
    }

    @Override
    public void configure(StateMachineTransitionConfigurer<OrderStatus, OrderEvent> transitions) throws Exception {
        transitions.withExternal()
                   .source(OrderStatus.NONE)
                   .target(OrderStatus.UNPAID)
                   .event(OrderEvent.CREATE)
                   .and()
                   .withExternal()
                   .source(OrderStatus.UNPAID)
                   .target(OrderStatus.CANCELED)
                   .event(OrderEvent.CANCEL)
                   .and()
                   .withExternal()
                   .source(OrderStatus.UNPAID)
                   .target(OrderStatus.PAID)
                   .event(OrderEvent.PAY)
                   .and()
                   .withExternal()
                   .source(OrderStatus.PAID)
                   .target(OrderStatus.DELIVERED)
                   .event(OrderEvent.DELIVER)
                   .and()
                   .withExternal()
                   .source(OrderStatus.DELIVERED)
                   .target(OrderStatus.FINISHED)
                   .event(OrderEvent.RECEIVE);
    }
}
