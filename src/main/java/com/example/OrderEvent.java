package com.example;

/**
 * 订单事件类型
 *
 * @author wangruiv on 2017-3-23 15:14:05
 */
public enum OrderEvent {
    /**
     * 创建
     */
    CREATE,

    /**
     * 支付
     */
    PAY,

    /**
     * 发货
     */
    DELIVER,

    /**
     * 收货
     */
    RECEIVE,

    /**
     * 取消
     */
    CANCEL
}
